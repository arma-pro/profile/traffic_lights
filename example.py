from src.traffic import generate
from src.main import run

if __name__ == '__main__':
    run(generate)
