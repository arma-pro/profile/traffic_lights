# Traffic lights

Пробная работа по написанию первой нейросети глубокого обучения.

## Задача

Реализовать возможность переключения светофоров в зависимости от нагруженности

## Быстрый старт

- устанавливаем python 3+
- клонируем репозиторий к себе
- создаем виртуальную среду с помощью [pipenv](https://pipenv.pypa.io/en/latest/) или [virtualenv](https://virtualenv.pypa.io/en/latest/) (по желанию)
- выполняем установку дополнительных пакетов: `pip install -r req.txt`
- настраиваем файл `configs/main.json`, если нужна своя конфигурация опыта(но для первого раза, хватит и того что есть)
- и выполняем `python example.py`

## Документация

[см. тут](docs/index.md)

## ToDo

- Данные для обучения сети брал из головы, поэтому могу не сильно совпадать с действительностью. Так что было бы неплохо собрать информацию из компетентных источников.
- Более хитрый генератор трафика, для проверки работы модели. Сейчас - это просто генератор случайных чисел.
