import json
import time

from src.helper import neural_network


def run(callback_traffic):
    """ Запускаем эксперимент обновления светофоров
    :param callback_traffic: функция получения трафика на светофорах
    :return:
    """
    config = json.load(open('configs/main.json'))
    model = json.load(open('configs/model.json'))

    start_time = time.time()
    end_time = start_time + config['example_time']

    traffic_lights = {}

    # соберем массив данных светофора
    for light_num in config['traffic_lights']:
        traffic_lights[light_num] = {
            'parent_id': config['traffic_lights'][light_num]['parent_id'],
            'last_change': start_time,
            'input': {},
            'pred': 0,
        }

        for label in model['label']:
            traffic_lights[light_num]['input'][label] = 0

        # Здесь будет записан итог гадания модели
        traffic_lights[light_num]['pred'] = 0

    while end_time >= time.time():
        traffic_lights = callback_traffic(traffic_lights)

        for light_num in traffic_lights:

            # время, когда светофор не работает на главно дороге
            time_red = config['traffic_lights'][light_num]['timing']['red']
            # время, когда светофор работает на главной дороге
            time_green = config['traffic_lights'][light_num]['timing']['green']

            # вычисляем время работы от последнего изменения статуса
            traffic_lights[light_num]['input']['time'] = time.time() - traffic_lights[light_num]['last_change']

            # вычисляем вероятность смены состояния светофора(сама нейронная сеть)
            traffic_lights[light_num]['pred'] = neural_network(
                list(traffic_lights[light_num]['input'].values()),
                model['weights']
            )

            # если процент переключения больше минимума, то переключаем
            if float(traffic_lights[light_num]['pred']) >= config['min_for_change']:
                change = False

                # сверяем время работы светофора, чтобы точно решить включать или выключать
                if traffic_lights[light_num]['input']['open'] == 1:
                    if float(time.time()) - float(traffic_lights[light_num]['last_change']) >= time_green:
                        traffic_lights[light_num]['input']['open'] = 0
                        change = True
                elif float(time.time()) - float(traffic_lights[light_num]['last_change']) >= time_red:
                    traffic_lights[light_num]['input']['open'] = 1
                    change = True

                # Если сменили состояние светофора, то обнулим данные по времени
                if change:
                    traffic_lights[light_num]['input']['time'] = 0
                    traffic_lights[light_num]['last_change'] = time.time()

                    print('Переключение светофора №' + str(light_num) + ': '
                          + ('ON' if traffic_lights[light_num]['input']['open'] else 'OFF'))

        # print(json.dumps(traffic_lights, indent=4))
        statuses = []
        for light_num in traffic_lights:
            statuses.append(traffic_lights[light_num]['input']['open'])
        print(statuses)

        # выдерживаем перерыв между вычислениями(пинг)
        time.sleep(config['ping'])

    print(traffic_lights)
