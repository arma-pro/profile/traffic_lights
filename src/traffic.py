import json
import time

from random import randint
from math import ceil


def generate(traffic_lights=None):
    config = json.load(open('configs/traffic.json'))

    if traffic_lights is None:
        return traffic_lights

    def calculate_time_diff(last_time: float) -> float:
        """
        Рассчитываем процент выполнения максимальной "скорости"

        :param last_time: время последнего изменения
        :return:
        """

        # время когда мы получаем максимальный прирост - "скорость"
        speed = float(config['speed'])

        # вычисляем время работы
        worck_time = float(time.time()) - last_time

        # если светофор еще не пораотал, то вернем 0 заплнение
        if worck_time == 0:
            return 0;

        # нам важен период только в пределах "скорости"
        if worck_time > speed:
            worck_time = worck_time % speed

        return worck_time / speed

    def calculate_load_diff(time_diff: float, max_load: int) -> int:
        """
        рассчитываем

        :param time_diff: процент выполнения максимальной "скорости"
        :param max_load: максимальное заполнение
        :return:
        """

        end = int(ceil(max_load * time_diff))

        return randint(0, end)

    def traffic(light_num: int):
        """
        Собираем данные по трафику на главной дороге

        :param light_num: номер светофора
        """

        time_diff = calculate_time_diff(float(traffic_lights[light_num]['last_change']))

        if traffic_lights[light_num]['input']['open'] == 0:
            max_load = config['traffic_lights'][str(light_num)]['passability'][0]
            traffic_lights[light_num]['input']['cur_load'] += calculate_load_diff(time_diff, max_load)
        else:
            max_load = config['traffic_lights'][str(light_num)]['passability'][1]
            traffic_lights[light_num]['input']['cur_load'] -= calculate_load_diff(time_diff, max_load)

        parent_id = traffic_lights[light_num]['parent_id']
        if parent_id > 0:
            traffic_lights[light_num]['input']['parent_load'] += traffic_lights[str(parent_id)]['input']['parent_load']

    def crosswalk(light_num: int):
        """
        Собираем данные по кол-ву пешеходов

        :param light_num: номер светофора
        """

        time_diff = calculate_time_diff(float(traffic_lights[light_num]['last_change']))

        if traffic_lights[light_num]['input']['open'] == 0:
            max_load = config['traffic_lights'][str(light_num)]['crosswalk'][0]
            traffic_lights[light_num]['input']['crosswalk_load'] += calculate_load_diff(time_diff, max_load)
        else:
            max_load = config['traffic_lights'][str(light_num)]['crosswalk'][1]
            traffic_lights[light_num]['input']['crosswalk_load'] -= calculate_load_diff(time_diff, max_load)

    def crossroads(light_num: int):
        """
        Собираем данные по трафику на второстепенном дороге перекрестка

        :param light_num: номер светофора
        """

        time_diff = calculate_time_diff(float(traffic_lights[light_num]['last_change']))

        if traffic_lights[light_num]['input']['open'] == 0:
            max_load = config['traffic_lights'][str(light_num)]['crossroads'][0]
            traffic_lights[light_num]['input']['crosswalk_load'] += calculate_load_diff(time_diff, max_load)
        else:
            max_load = config['traffic_lights'][str(light_num)]['crossroads'][1]
            traffic_lights[light_num]['input']['crossroads_load'] -= calculate_load_diff(time_diff, max_load)

    for light_num in traffic_lights:
        traffic(light_num)
        crosswalk(light_num)
        crossroads(light_num)

    return traffic_lights
