def neural_network(input, weights):
    """ Вычисление результата нейронной сети
    :param input: входящие параметры предсказания
    :param weights: веса нейрона
    :return:
    """
    out = 0

    assert len(input) == len(weights), 'Длина входящего списка, должна соответствовать длине списка весов'

    for i in range(len(input)):
        out += input[i] * weights[i]

    return out


def ele_mul(scalar, vector):
    """ Умножение скаляра на список
    :param scalar: Скаляр
    :param vector: Список
    :return:
    """
    out = [0 for _ in range(len(vector))]

    for i in range(len(out)):
        out[i] = scalar * vector[i]

    return out
