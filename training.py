from src.helper import neural_network, ele_mul
import json


def main(config):
    # альфа-коэффициент - скорость спуска
    alpha = config['alpha']
    # массив начальных весов для обучения
    weights = config['model']

    # Номер последнего элемента - это результат
    end = len(config['items'][0]) - 1

    for e in range(config['epochs']):
        print(weights)

        # делаем столько итераций спуска, сколько задано в настройках
        for i in range(config['errors_iter']):

            # перебираем тестовые данные
            for k in range(len(config['items'])):
                # Входные параметры
                input = config['items'][k][0:end]
                # Ожидаемый результат
                goal = config['items'][k][end]

                # проверим на всякий случай размеры списков
                assert len(input) == len(weights), 'Размер списка входных параметров, должен быть равен размеру списка весов'

                # Вычисляем получаемое значение сети
                pred = neural_network(input, weights)

                # вычисляем дельту и ошибку
                delta = (pred - goal)
                error = delta ** 2

                # получаем список дельт весов
                weight_delta = ele_mul(delta, input)

                # глушим ненужные веса
                for n in range(len(config['mute_deltas'])):
                    weight_delta[config['mute_deltas'][n]] = 0

                # print("Iter: " + str(i))
                # print("pred: " + str(pred))
                # print("error: " + str(error))
                # print("delta: " + str(delta))
                # print("input: " + str(input))
                # print("weights: " + str(weights))
                # print("weight_delta: " + str(weight_delta))

                # обновляем список весов
                for j in range(len(weights)):
                    weights[j] -= alpha * weight_delta[j]

    print("\r\n\r\nEND weights: " + str(weights))

    model = {
        "label": config['items_label'][0:end],
        "weights": weights
    }

    # сохраняем получившийся результат в файл конфигурации модели
    with open('configs/model.json', 'w', encoding='utf-8') as f:
        json.dump(model, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    config_train = json.load(open('configs/training.json'))

    main(config_train)
