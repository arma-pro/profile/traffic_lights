# Документация

## Список config-файлов

* [main.json](configs/main.json.md) - Основные настройки
* [model.json](configs/model.json.md) - Модель
* [traffic.json](configs/traffic.json.md) - Настройки генератора траффика
* [training.json](configs/training.json.md) - Настройки тренировки модели
